import random
import configparser
import json
from typing import Optional, List, Any, Union

import six
from telethon import TelegramClient
from telethon.errors import SessionPasswordNeededError
from telethon.sync import TelegramClient
from telethon import functions, types
from telethon.tl.functions.messages import SearchRequest
from telethon.tl.types import InputMessagesFilterEmpty
from pip._vendor import requests
from googletrans import Translator

translator = Translator()

# Reading Configs
config = configparser.ConfigParser()
config.read("config.ini")

api_id = config['Telegram']['api_id']
api_hash = config['Telegram']['api_hash']

phone = config['Telegram']['phone']
username = config['Telegram']['username']
client = TelegramClient(username, api_id, api_hash)

url = "https://api.telegram.org/bot1264507916:AAGliFj8ESIaA9t3jXl4-0rjB9h0QUkFemU/"


# create func that get chat id
def get_chat_id(update):
    chat_id = update['message']["chat"]["id"]
    return chat_id

#Translate text from english to chinese
def translate_text_chinese(text):
    ttext = translator.translate(text, dest='zh-tw')
    return ttext.text

#Translate text from english to chinese
def translate_text_english(text):
    ttext = translator.translate(text, dest='en')
    return ttext.text

# create function that get message text
def get_message_text(update):
    message_text = update["message"]["text"]
    return message_text


# create function that get last_update
def last_update(req):
    response = requests.get(req + "getUpdates")
    response = response.json()
    result = response["result"]
    total_updates = len(result) - 1
    return result[total_updates]  # get last record message update


# create function that let bot send message to user
def send_message(chat_id, message_text):
    params = {"chat_id": chat_id, "text": message_text, "parse_mode": 'HTML'}
    response = requests.post(url + "sendMessage", data=params)
    return response


def concatenate_list_data(list):
    groups = []

    html = '🎰鴨博娛樂｜富貴享樂👯‍♀\n'
    html = html + '注册即享：👙黃片看不盡👙 • 🧧 注册发财金88🧧 • 💰高額续充彩1998 💰'
    html = html + '<b><a href="https://yaboxxx999.com/a/35702756">鴨博富貴人生註冊鏈結</a></b>\n'
    html = html + '🦆鴨博享樂生活 資訊交流小站🦆'
    html = html + '<b><a href="https://yabo3.webnode.tw/">鴨博獨家優惠、體壇最火動態、最深成人資訊。</a></b>\n'
    html = html + '1. <b><a href="https://t.me/YABO_TOOLS">鸭博娱乐-资源群_狗推/刷子/名单/工具/BC/WZ</a></b>\n'
    html = html + '2. <b><a href="https://t.me/YABO_PARTNER">鸭博娱乐-求才群_招贤纳士/招聘徴才</a></b>\n'
    html = html + '3. <b><a href="https://t.me/yaboxxx1">鸭博娱乐 /白金VIP /代理交流群 /代理 /菠菜 /佣金 /色片 /黄片 /福利 /免费 /视频 /直播 /鸭脖 /亚博 /BBIN</a></b>\n'
    html = html + '4. <b><a href="https://t.me/yaboxxxxx">鸭博娱乐合作群 yabo.xxx</a></b>\n'
    html = html + '5. <b><a href="https://t.me/jsi88_video">就是爱啪啪老司机群</a></b>\n'
    i = 6
    random.shuffle(list)
    for i, x in enumerate(list, start=6):
        if i <= 20:
            ctext = translate_text_chinese(x['title'])
            html = html + "{}.{}".format(i, ' <a href="https://t.me/'+str(x['username'])+'"><b>' + x['title'] + '</b>--' + str(
             x['members']) + '</a>\n')

    return html


async def main():
    await client.start()
    # await client.send_message('me', 'Type a keyword to search channels/groups')
    # await client.send_message('me', 'Type a keyword to search channels/groups')
    # print(result.chats[0].id)

    update_id = last_update(url)["update_id"]
    while True:
        update = last_update(url)
        # send_message(get_chat_id(update), 'Type a keyword to search channels/groups')
        if update_id == update["update_id"]:

            if get_message_text(update).lower() != "" or get_message_text(
                    update) != "Type a keyword to search channels/groups":
                search = get_message_text(update).lower()

                etext = translate_text_english(search)

                urlc = "https://cn.tgstat.com/channels/list"
                payload = "q=" + str(search) + "&country=cn&category=adult&language=chinese&offset=20&peerType=channel&extendedSyntax=0&token=0247d7ea68d7471945237a352adffd51c"
                headers = {
                    'content-type': "application/x-www-form-urlencoded",
                    'cache-control': "no-cache"
                }
                response = requests.request("POST", urlc, data=payload.encode('utf-8'), headers=headers)
                data = json.loads(response.text)
                print(data["items"]['list'])

                send_message(get_chat_id(update), concatenate_list_data(data["items"]['list']))
            elif get_message_text(update).lower() == "play":
                _1 = random.randint(1, 6)
                _2 = random.randint(1, 6)
                _3 = random.randint(1, 6)
                send_message(get_chat_id(update),
                             'You have ' + str(_1) + ' and ' + str(_2) + ' and ' + str(_3) + ' !\n Your result is ' +
                             str(_1 + _2 + _3) + '!!!')
            else:
                send_message(get_chat_id(update), "Sorry Not Understand what you inputted:( I love you")
            update_id += 1


with client:
    client.loop.run_until_complete(main())

