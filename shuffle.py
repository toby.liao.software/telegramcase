import random, sys

filename = sys.argv[1]
fid = open(filename, "r")
li = fid.readlines()
header = li[0]
content = li[1:]
fid.close()

random.shuffle(content)
content2write = []
for idx, line in enumerate(content):
    token = line.strip().split(",")[1:]
    content2write.append("{},{},{},{},{},{},{},{}".format(idx, *token))

with open(filename, 'w') as write_file:
    write_file.write(header)
    for line in content2write:
        write_file.write(line+'\n')  