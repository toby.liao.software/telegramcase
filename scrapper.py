from telethon.sync import TelegramClient

import csv


api_id = 1470407
api_hash = '80c26b2ccf9ecf986711f03aafd2826e'
phone = '+919647796911'

session_name = '+919647796911'

client = TelegramClient(str(session_name), api_id, api_hash)

client.connect()
if not client.is_user_authorized():
    client.send_code_request(phone)
    client.sign_in(phone, input('Enter the code: '))

print('Fetching Members...')
all_participants = []

#enter target group or channel
target = 'https://t.me/bcshequn'

all_participants = client.get_participants(target, aggressive=True)

print('Saving In file...')
with open("Members1.csv", "w", encoding='UTF-8') as f:
    writer = csv.writer(f, delimiter=",", lineterminator="\n")
    writer.writerow(['sr. no.','username', 'user id', 'access hash', 'name', 'group', 'group id'])
    i = 0
    for user in all_participants:

        i += 1
        if user.username:
            username = user.username
        else:
            username = ""
        if user.first_name:
            first_name = user.first_name
        else:
            first_name = ""
        if user.last_name:
            last_name = user.last_name
        else:
            last_name = ""
        name = (first_name + ' ' + last_name).strip()
        writer.writerow([i,username, user.id, user.access_hash, name, 'group name', 'groupid'])
        print('Members scraped successfully.')

